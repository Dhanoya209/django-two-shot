from django.db import models
from django.conf import settings

# Create your models here.


# Expense Category model
class ExpenseCategory(models.Model):
    # name max length 50
    name = models.CharField(max_length=50)
    # an owner with foreign key to user
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        # related name of categories
        related_name="categories",
        # cascade delete
        on_delete=models.CASCADE,
    )

    # a __str__ method that returns name value
    def __str__(self):
        return self.name


# Account model
class Account(models.Model):
    # name max length 100
    name = models.CharField(max_length=100)
    # number that has chars with max 20
    number = models.CharField(max_length=20)
    # an owner with foreign key to user
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        # related name of accounts
        related_name="accounts",
        # cascade delete
        on_delete=models.CASCADE,
    )

    # a __str__ method that returns name value
    def __str__(self):
        return self.name


# Receipt model
class Receipt(models.Model):
    # vendor chars max length 200
    vendor = models.CharField(max_length=200)
    # total decimalFeild
    total = models.DecimalField(max_digits=10, decimal_places=3)
    # three decimal places
    # max 10 digits
    # tax decimalFeild
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    # three decimals
    # max 10 digits
    # date of when transaction tokk place
    date = models.DateTimeField()

    # purchaser with foreign ket to user
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        # a related name of receipts
        related_name="receipts",
        # cascade delete
        on_delete=models.CASCADE,
    )

    # category with foreign ket to ExpenseCategory model
    category = models.ForeignKey(
        "ExpenseCategory",
        # reletated name receipts
        related_name="receipts",
        # cascade delete
        on_delete=models.CASCADE,
    )

    # account with foreign key to Account model
    account = models.ForeignKey(
        "Account",
        # a related name to receipts
        related_name="receipts",
        # cascade delete
        on_delete=models.CASCADE,
        # alow to be null
        null=True,
    )
