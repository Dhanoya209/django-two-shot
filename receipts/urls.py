from django.urls import path

from receipts.views import (
    ExpenseCategoryListView,
    AccountListView,
    ReceiptListView,
    ReceiptCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("accounts/", AccountListView.as_view(), name="list_accounts"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
]
